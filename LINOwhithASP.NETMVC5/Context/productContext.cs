﻿using LINOwhithASP.NETMVC5.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LINOwhithASP.NETMVC5.Context
{
    public class productContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}